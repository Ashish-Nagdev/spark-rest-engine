package com.orbigo.api.spark.rest.options

import java.util.{Locale, Properties}


/**
  * @Author Ashish Nagdev
 * Options for the REST data source.
 */


class RESTOptions(@transient private val parameters: Map[String, String]) extends Serializable {

  import RESTOptions._

  // def this(parameters: Map[String, String]) = this(parameters)

  def this(url: String, input: String, parameters: Map[String, String]) = {
    // this(CaseInsensitiveMap(parameters ++ Map(
    this(parameters ++ Map(
      RESTOptions.REST_URL -> url,
      RESTOptions.REST_INPUT -> input))
  }

  val asProperties: Properties = {
    val properties = new Properties()
    parameters.foreach { case (k, v) => properties.setProperty(k, v) }
    properties
  }

  /**
    * This library supports several options for calling the target REST service:
    * `url`: This is the uri of the target micro service. You can also provide the common parameters (those that don't vary with each API call) in this url. This is a mandatory parameter.
    * `input`: You need to pass the name of the Temporary Spark Table which contains the input parameters set. This is a mandatory parameter too.
    * `method`: The supported http/https method. Possible types supported right now are `POST`, and `GET`. Default is `POST`
    * `userId` : The userId in case the target API needs basic authentication.
    * `userPassword` : The password in case the target API needs basic authentication
    * `partitions`: Number of partition to be used to increase parallelism. Default is 2.
    * `connectionTimeout` : In case the target API needs high time to connect. Default is 1000 (in ms)
    * `readTimeout` : In case the target API returns large volume of data which needs more read time. Default is 5000 (in ms)
    * `schemaSamplePcnt` : Percentage of records in the input table to be used to infer the schema. The default is "30" and minimum is 3. Increase this number in case you are getting an error or the schema is not propery inferred.
    * `callStrictlyOnce` : This value can be used to ensure the backend API is called only once for each set of input parameters. The default is "N", allowing the back end API to get called for multiple times - once for inferring the schema and then for other operations. If this value is set to "Y" the backend API would be called only once (during infering the schema) for all of the input parameter sets and would be cached. This option is useful when the target APIs are paid service or does not support calls per day/per hour beyond certain number. However, the results would be cached which will increase the memory usage.
    *
    */

  /*
     Required Parameters
  */

  require(parameters.isDefinedAt(REST_URL), s"Option '$REST_URL' is required.")
  require(parameters.isDefinedAt(REST_INPUT), s"Option '$REST_INPUT' is required.")

  val url = parameters(REST_URL)
  val input = parameters(REST_INPUT)

  /*
     Optional Parameters

  */

  val inputType = parameters.getOrElse(REST_INPUT_TYPE, "tableName")
  val authType = parameters.getOrElse(REST_AUTH_TYPE, "Basic")
  val userId = parameters.getOrElse(REST_USER_ID, "")
  val userPassword = parameters.getOrElse(REST_USER_PASSWORD, "")
  val method = parameters.getOrElse(REST_METHOD, "POST")
  val postInputFormat = parameters.getOrElse(REST_POST_INPUT_FORMAT, "json")
  val inputKeys = parameters.getOrElse(REST_INPUT_KEYS, "")
  val outputFormat = parameters.getOrElse(REST_OUTPUT_FORMAT, "json")
  val readTimeout = parameters.getOrElse(REST_READ_TIMEOUT, "5000")
  val connectionTimeout = parameters.getOrElse(REST_CONNECTION_TIMEOUT, "1000")
  val inputStrRecordDelimeter = parameters.getOrElse(REST_INPUT_STR_RECORD_DELIMETER, ";")
  val inputStrFieldDelimeter = parameters.getOrElse(REST_INPUT_STR_FIELD_DELIMETER, "#")
  val inputPartitions = parameters.getOrElse(REST_INPUT_PARTITIONS, "2")
  val includeInputsInOutput = parameters.getOrElse(REST_INPUT_INCLUDE, "Y")
  val oauthConsumerKey = parameters.getOrElse(REST_OAUTH1_CONSUMER_KEY, "")
  val oauthConsumerSecret = parameters.getOrElse(REST_OAUTH1_CONSUMER_SECRET, "")
  val oauthToken = parameters.getOrElse(REST_OAUTH1_TOKEN, "")
  val oauthTokenSecret = parameters.getOrElse(REST_OAUTH1_TOKEN_SECRET, "")
  val callStrictlyOnce = parameters.getOrElse(REST_CALL_STRICTLY_ONCE, "N")
  val schemaSamplePcnt = parameters.getOrElse(REST_SCHEMA_SAMPLE_PCNT, "30")

}

object RESTOptions {
  private val restOptionNames = collection.mutable.Set[String]()

  private def newOption(name: String): String = {
    restOptionNames += name.toLowerCase(Locale.ROOT)
    name
  }

  val REST_URL = newOption("url")
  val REST_INPUT_TYPE = newOption("inputType")
  val REST_INPUT = newOption("input")
  val REST_POST_INPUT_FORMAT = newOption("postInputFormat")
  val REST_INPUT_KEYS = newOption("inputKeys")
  val REST_USER_ID = newOption("userId")
  val REST_USER_PASSWORD = newOption("userPassword")
  val REST_METHOD = newOption("method")
  val REST_OUTPUT_FORMAT = newOption("outputFormat")
  val REST_READ_TIMEOUT = newOption("readTimeOut")
  val REST_CONNECTION_TIMEOUT = newOption("connectionTimeOut")
  val REST_INPUT_STR_RECORD_DELIMETER = newOption("inputStrRecordDelimeter")
  val REST_INPUT_STR_FIELD_DELIMETER = newOption("inputStrFieldDelimeter")
  val REST_INPUT_PARTITIONS = newOption("inputPartitions")
  val REST_INPUT_INCLUDE = newOption("includeInputsInOutput")
  val REST_AUTH_TYPE = newOption("authType")
  val REST_OAUTH1_CONSUMER_KEY = newOption("oauthConsumerKey")
  val REST_OAUTH1_CONSUMER_SECRET = newOption("oauthConsumerSecret")
  val REST_OAUTH1_TOKEN = newOption("oauthToken")
  val REST_OAUTH1_TOKEN_SECRET = newOption("oauthTokenSecret")
  val REST_CALL_STRICTLY_ONCE = newOption("callStrictlyOnce")
  val REST_SCHEMA_SAMPLE_PCNT = newOption("schemaSamplePcnt")
}
