package com.orbigo.api.spark.rest.datasource

import com.orbigo.api.spark.rest.options.RESTOptions
import com.orbigo.api.spark.rest.relation.RESTRelation
import org.apache.spark.sql.SQLContext
import org.apache.spark.sql.sources.{BaseRelation, DataSourceRegister, RelationProvider}

/**
 * @Author Ashish Nagdev

 *  This is the implementation of the interface needed
 *  for DataFrameReader class to load this Data Source
*/

class RESTDataSource extends RelationProvider
  with DataSourceRegister {

  override def shortName(): String = "rest"

  override def createRelation(
      sqlContext: SQLContext,
      parameters: Map[String, String]): BaseRelation = {

    val restOptions = new RESTOptions(parameters)

    RESTRelation(restOptions)(sqlContext.sparkSession)
  }
}

