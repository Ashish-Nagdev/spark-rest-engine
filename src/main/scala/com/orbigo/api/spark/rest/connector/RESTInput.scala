package com.orbigo.api.spark.rest.connector

import java.net.URLEncoder
import scala.collection.mutable.ArrayBuffer


/**
  * @Author Ashish Nagdev

  *  This is the implementation of the Input to REST API for POST operations
  */
object RESTInput {

  def prepareJsonInput(keys: Array[String], values: Array[String]) : String = {

    val keysLength = keys.length
    var cnt = 0
    val outArrB : ArrayBuffer[String] = new ArrayBuffer[String](keysLength)

    while (cnt < keysLength) {
      outArrB += "\"" + keys(cnt) + "\":\"" + values(cnt) + "\""
      cnt += 1
    }

    "{" + outArrB.mkString(",") + "}"

  }

  def prepareTextInput(keys: Array[String], values: Array[String]) : String = {

    val keysLength = keys.length
    var cnt = 0
    val outArrB : ArrayBuffer[String] = new ArrayBuffer[String](keysLength)

    while (cnt < keysLength) {
      outArrB += URLEncoder.encode(keys(cnt)) + "=" + URLEncoder.encode(values(cnt))
      cnt += 1
    }

    outArrB.mkString("&")

  }

}
