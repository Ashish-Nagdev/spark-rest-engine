package com.orbigo.api.spark.rest.connector

import scala.collection.mutable.ArrayBuffer

/**
  * @Author Ashish Nagdev

  *  This is the implementation of the Output of REST API for GET operations
  */
object RESTOutput {

  def prepareJsonOutput(keys: Array[String], values: Array[String], resp: String) : String = {

    val keysLength = keys.length
    var cnt = 0
    val outArrB : ArrayBuffer[String] = new ArrayBuffer[String](keysLength)

    while (cnt < keysLength) {
      outArrB += "\"" + keys(cnt) + "\":\"" + values(cnt) + "\""
      cnt += 1
    }

    "{" + outArrB.mkString(",") +  ",\"output\":" + resp + "}"

  }

}
