# Apache Spark based Rest Data Engine

- This is a library for calling REST based services/APIs for multiple sets of input parameters in parallel and collating the results, returned by the REST service, in a Dataframe.

- REST based services (for example Address Validation services, Google Search API, etc.) typically take only one set of input parameters at a time and return the corresponding record(s). However for some cases, the same API needs to be called multiple times to account for a large set of different input parameters (e.g. validating the addresses for a set of target customers; getting personality insights from thousands of Tweets; etc.)

- This package supports calling the target service API in a distributed way for different input parameter sets. The results are returned in a DataFrame in a Structure specific to the API without the user specifying this schema.


## Building the jar file

In case you want to build the jar file yourself try the following steps. 

```
mvn clean install -Dscalastyle.skip=true
```

## Using the jar file with Spark shell
This package can be added to  Spark using the `--packages` of `--jars` command line option.  For example, to include it when starting the spark shell as jar from the same folder use :

```
$SPARK_HOME/bin/spark-shell --jars spark-rest-engine-2.11-1.1.0-SNAPSHOT.jar --packages org.scalaj:scalaj-http_2.10:2.3.0

```

## Features
This package allows multiple calls, in parallel, to a target REST based micro service for a set of different input parameters. These parameters can be passed as a Temporary Spark Table where the column names of the table should be same as the keys of the target API. Each row in the table, and corresponding combination of parameter values, will be used to make one API call. The result from the multiple calls to the API is returned as a [Spark DataFrame](https://spark.apache.org/docs/1.6.0/sql-programming-guide.html) of Rows with an output structure matching that of the target API's response.  

This library supports several options for calling the target REST service:
* `url`: This is the uri of the target micro service. You can also provide the common parameters (those that don't vary with each API call) in this url. This is a mandatory parameter.
* `input`: You need to pass the name of the Temporary Spark Table which contains the input parameters set. This is a mandatory parameter too.
* `method`: The supported http/https method. Possible types supported right now are `POST`, and `GET`. Default is `POST`
* `userId` : The userId in case the target API needs basic authentication.
* `userPassword` : The password in case the target API needs basic authentication
* `partitions`: Number of partition to be used to increase parallelism. Default is 2.
* `connectionTimeout` : In case the target API needs high time to connect. Default is 1000 (in ms)
* `readTimeout` : In case the target API returns large volume of data which needs more read time. Default is 5000 (in ms)
* `schemaSamplePcnt` : Percentage of records in the input table to be used to infer the schema. The default is "30" and minimum is 3. Increase this number in case you are getting an error or the schema is not propery inferred.
* `callStrictlyOnce` : This value can be used to ensure the backend API is called only once for each set of input parameters. The default is "N", allowing the back end API to get called for multiple times - once for inferring the schema and then for other operations. If this value is set to "Y" the backend API would be called only once (during infering the schema) for all of the input parameter sets and would be cached. This option is useful when the target APIs are paid service or does not support calls per day/per hour beyond certain number. However, the results would be cached which will increase the memory usage.

## Typical Structure of the Dataframe returned by Rest Data Source

The dataframe created by this REST Data Source will return a set of Rows of the same Structure. The Structure will contain the input fields that were used for the API call as well as the returned output under a new column named 'output'. Whatever gets returned in 'output' is specific to the target REST API. It's structure can be easily obtained by printSchema method of Dataframe.

Below is an example of the structure returned using REST Data Source with the [Watson API for Natural Language Understanding with sentiment as feature] (https://www.ibm.com/watson/developercloud/natural-language-understanding/api/v1/).

```scala

root
 |-- output: struct (nullable = true)
 |    |-- code: long (nullable = true)
 |    |-- error: string (nullable = true)
 |    |-- language: string (nullable = true)
 |    |-- retrieved_url: string (nullable = true)
 |    |-- sentiment: struct (nullable = true)
 |    |    |-- document: struct (nullable = true)
 |    |    |    |-- label: string (nullable = true)
 |    |    |    |-- score: double (nullable = true)
 |    |-- usage: struct (nullable = true)
 |    |    |-- features: long (nullable = true)
 |    |    |-- text_characters: long (nullable = true)
 |    |    |-- text_units: long (nullable = true)
 |-- q: string (nullable = true)
 |-- url: string (nullable = true)
 
 ```
 
In this example, 'q' and 'url' are the input parameters passed to the service; the Temporary Spark Table used as input had two columns - 'q' and 'url'. Each row of that table had different values for 'q' and 'url'. The 'output' field contains the result returned by the Watson API for Natural Language Understanding.  

Sometimes there could be an additional field under root, namely '\_corrupt_records'. This field will contain the outputs for the records for which the API returned an error.


